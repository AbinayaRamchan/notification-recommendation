require 'fcm'
require 'yaml'
require 'pg'


class FcmNotification
  
  def api_key
    @config['api_key']
  end

  def initialize
    case ARGV[0]
      when "--production"
       @config1 = YAML.load_file('data.yml')
       @config = @config1['production']
      when "--development"
       @config1 = YAML.load_file('data.yml')
       @config = @config1['development']
      else
       @config1 = YAML.load_file('data.yml')
       @config = @config1['default']
    end

    @fcm = FCM.new(api_key)
    
  end

  def connect
   @con = PG.connect :dbname => @config['database'], :user => @config['username'], :password => @config['password']
  end

  def connect_close
    rescue PG::Error => e
    puts e.message 
    ensure
    @rs.clear if @rs
    @con.close if @con
  end

  def fcm_registration_ids
     @fcm_reg_ids = []
     connect

     @rs = @con.exec "SELECT * FROM users"

     @rs.each do |row|
       @fcm_reg_ids << row['gcm_registration_id']
     end

     connect_close
     #@fcm_reg_ids = ["e4BIbT0zcn4:APA91bFDGE3vlx6TEKCSAOjXRHZ1v7OjNxdFfWQFmR734qx-St-LEzSB-LRup-DRf0IAQKIcO_bEwp9BfvxMsEFyMYS--p7S6d7uPhIejeaToEg-Ty_Yg0Vr7ieRrY2BJcFPK6K-LvV_"]

     @fcm_reg_ids
  end


  def reco_options
    options = 
    {
      data: 
        { type: "recommendation", 
          version: "1.2.1", 
          notification_message: "SuperPlans are available !", 
          notification_description: "Save some money with SuperPlans"
        }
    }
  end

  def update_options
    options = 
    {
      data: 
      { type: "app_update", 
        version: "2.1.2", 
        notification_message: "New update available!!", 
        notification_description: "Update your Super Plan to get new features"
      }
    }
  end

  def survey_options
    options = 
    {
      data: 
      { type: "survey", 
        notification_message: "This weekend, you can help SuperPlan !", 
        notification_description: "Take up the survey and help us tune our product to serve you better."
      }
    }
  end

  def test_id_retrival
    puts fcm_registration_ids
  end

  def send_update_notifications
    response = @fcm.send(fcm_registration_ids, update_options)
  end

  def send_recommendation_notifications
    response = @fcm.send(fcm_registration_ids, reco_options)
  end

  def send_survey_notifications
    response = @fcm.send(fcm_registration_ids, survey_options)
  end

end

notify = FcmNotification.new
#puts notify.test_id_retrival
 puts notify.send_update_notifications
# puts notify.send_recommendation_notifications
# puts notify.send_survey_notifications
